# [1.4.0](https://gitlab.com/leader716/repo-configs/compare/v1.3.0...v1.4.0) (2020-02-04)


### Features

* test user configuration and multiple groups ([f6ffdda](https://gitlab.com/leader716/repo-configs/commit/f6ffdda))
* update webhook ([77f8ee4](https://gitlab.com/leader716/repo-configs/commit/77f8ee4))

# [1.3.0](https://gitlab.com/leader716/repo-configs/compare/v1.2.0...v1.3.0) (2020-02-04)


### Features

* test user configuration ([820f7f8](https://gitlab.com/leader716/repo-configs/commit/820f7f8))

# [1.2.0](https://gitlab.com/leader716/repo-configs/compare/v1.1.0...v1.2.0) (2020-02-03)


### Features

* update gitlab ci ([0518361](https://gitlab.com/leader716/repo-configs/commit/0518361))

# [1.1.0](https://gitlab.com/leader716/repo-configs/compare/v1.0.0...v1.1.0) (2020-02-03)


### Features

* test release ([6cc312b](https://gitlab.com/leader716/repo-configs/commit/6cc312b))

# 1.0.0 (2020-02-03)


### Features

* initial commit ([c6b2e26](https://gitlab.com/leader716/repo-configs/commit/c6b2e26))
